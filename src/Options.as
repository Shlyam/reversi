package
{
	public class Options
	{
		static public var WIDTH:uint = 4;
		static public var HEIGHT:uint = 4;
		
		static public var CELL_SIZE:Number = 40;
		
		static public const FRAME_THICKNESS:Number = 1;
		static public var FRAME_COLOR:uint = 0x00;
		static public var FIELD_COLOR:uint = 0x1A8A57;
		
		static public var AI_DIFFICULT:uint = 2;
		static public var DISCS_COLOR:uint = 1;
	}
}