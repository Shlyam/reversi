package ai
{
	import flash.geom.Point;
	
	import algorithm.MinMax;
	
	import constant.CellType;
	
	import events.GridEvent;
	
	import model.GridModel;
	
	import starling.events.Event;
	import starling.events.EventDispatcher;
	
	public class AIModel extends EventDispatcher
	{
		static public var INSTANCE:AIModel;
		
		private var _gridModel:GridModel;
		private var _allowSteps:Vector.<Point>;
		private var _minMax:MinMax;
		
		
		
		public function AIModel(gridModel:GridModel)
		{
			INSTANCE = this;
			_gridModel = gridModel;
			_gridModel.addEventListener(GridEvent.FOND_POSSIBLE, getAllowedSteps);
			_minMax = new MinMax();
			
			_allowSteps = new Vector.<Point>();
		}
		
		private function getAllowedSteps(e:Event):void
		{
			if(!_gridModel.isStart) _allowSteps.push( new Point(e.data.x, e.data.y) );	
		}
		
		public function makeAIStep(e:Event):void
		{
			
			if(_allowSteps.length > 0)
			{
				if(Options.AI_DIFFICULT == 0)
				{
					var randStep:uint = uint(_allowSteps.length*Math.random());
					_gridModel.todoStep(_allowSteps[randStep].x, _allowSteps[randStep].y); 
				}
				else
				{
					if(Options.AI_DIFFICULT == 1) MinMax.MAX_DEPTH = 7;
					else MinMax.MAX_DEPTH = 11;
					
					var pt:Point = _minMax.startFind(CellType.WHITE, _gridModel.grid)
					if(pt) _gridModel.todoStep(pt.y, pt.x);
				}
				_allowSteps = new Vector.<Point>();			
			}
		}
		
		public function removeListeners():void
		{
			_gridModel.removeEventListener(GridEvent.FOND_POSSIBLE, getAllowedSteps);		
		}
	}
}