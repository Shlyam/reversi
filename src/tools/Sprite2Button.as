// =================================================================================================
//
//	Simple Assets Engine
//	Copyright 2015 Shlyam Aleksey. All Rights Reserved.
//
//	This program is free software. You can redistribute and/or modify it
//	in accordance with the terms of the accompanying license agreement.
//
// =================================================================================================
package tools
{
	import com.greensock.TweenLite;
	
	import tools.display.SimpleImage;
	
	import starling.display.DisplayObject;
	import starling.display.Sprite;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	/**	Позволяет переобразовать экземпляр класса SimpleImage в анимированую кнопку
	 *  и вызвать trigger-функцию
	 */
	public class Sprite2Button extends Sprite
	{
		static public var SCALE:String = "SCALE";
		static public var BRIGHT:String = "COLOR";
		
		private var __onTriggered:Function;
		private var __sprite:SimpleImage;

		private var __scale:Number;
		private var __filter:ColorFilter;
		private var __mobile:Boolean;
		
		/**Создает экземпляр Sprite2Button
		 * 
         *  @param image   		  	Экземпляр класса SimpleImage необходимый для переобразования
         *  @param onTriggered    	Callback-функция, выполняющаяся после события Trigger.
		 * 							Обязательно должна принимать параметр TouchEvent
         *  @param type       	  	Тип анимации для состояний Over и Down. SCALE - маштабирование, BRIGHT - засветление
         */
		public function Sprite2Button(image:SimpleImage, onTriggered:Function, type:String = "COLOR", mobile:Boolean = false):void
		{
			__sprite = image;
			__sprite.useHandCursor = true;
			__onTriggered = onTriggered;
			__mobile = mobile;
			
					image.alignPivot();
					image.x += image.width >> 1;
					image.y += image.height >> 1;
			switch(type)
			{
				case SCALE:
					__scale = image.scaleX;
					
					image.addEventListener( TouchEvent.TOUCH, onTouchScale);		
					break;
					
				case BRIGHT:
					__filter = new ColorFilter();
					image.filter = __filter;
					image.addEventListener( TouchEvent.TOUCH, onTouchColor);		
					break;
			}
		}	
		
		private function onTouchScale(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(e.currentTarget as DisplayObject);
			if (!touches.length) 
			{
				tween2Small();
				return;
			}
			
			for each (var touch:Touch in touches)
			{
				if(touch.phase == TouchPhase.HOVER && !__mobile) tween2Big();
				
				if(touch.phase == TouchPhase.BEGAN && __mobile) tween2VerySmall();
				else if(touch.phase == TouchPhase.BEGAN) tween2Small();
				
				if(touch.phase == TouchPhase.ENDED)
				{
					tween2Small();
					if(__onTriggered) __onTriggered(e);
				}
			}
		}
		
		private function onTouchColor(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(e.currentTarget as DisplayObject);
			if (!touches.length) 
			{
				tween2normal();
				return;
			}
			
			for each (var touch:Touch in touches)
			{
				if(touch.phase == TouchPhase.HOVER) tween2light();
				
				if(touch.phase == TouchPhase.BEGAN && __mobile) tween2light();
				else if(touch.phase == TouchPhase.BEGAN) tween2normal();
				
				if(touch.phase == TouchPhase.ENDED)
				{
					tween2normal();
					if(__onTriggered) __onTriggered(e);
				}
			}
		}
		
		private function tween2Big():void
		{
			TweenLite.to( __sprite, 0.2, { scaleX : __scale*1.25, scaleY : __scale*1.25	});
		}
		
		private function tween2VerySmall():void
		{
			TweenLite.to( __sprite, 0.2, { scaleX : __scale*0.75, scaleY : __scale*0.75	});
		}
		
		private function tween2Small():void
		{
			TweenLite.to( __sprite, 0.2, { scaleX : __scale, scaleY : __scale});
		}
		
		private function tween2light():void
		{
			TweenLite.to( __filter, 0.2, {	rr : 1200, 	rg : 0, 	rb : 0,
											gr : 0, 	gg : 1200, 	gb : 0,
											br : 0, 	bg : 0, 	bb : 1200, update: 1});
		}
		
		private function tween2normal():void
		{
			TweenLite.to( __filter, 0.2, {	rr : 1000, 	rg : 0, 	rb : 0,
											gr : 0, 	gg : 1000, 	gb : 0,
											br : 0, 	bg : 0, 	bb : 1000, update: 1});
		}
	}
}