package view
{
	import com.greensock.TweenLite;
	
	import flash.geom.Point;
	
	import ai.AIModel;
	
	import constant.Animations;
	import constant.CellType;
	
	import tools.display.SimpleImage;
	import tools.display.SimpleShape;
	import tools.display.SimpleSprite;
	
	import events.GridEvent;
	
	import model.GridModel;
	
	import starling.display.Canvas;
	import starling.events.Event;
	
	public class CellView extends SimpleSprite
	{		
		private const scale:Number = 0.02;
		
		private var _black:SimpleImage;
		private var _white:SimpleImage;
		private var _posible:Canvas;
		
		private var _gridModel:GridModel;
		private var _position:Point;
		
		private var startScale:Number = 0.5;
		
		public function CellView(gridModel:GridModel, x:Number, y:Number)
		{
			_gridModel = gridModel;
			_position = new Point(x, y)
		}
		
		override protected function init(e:Event = null):void
		{
			super.init(e);
			
			drawFrame();
			drawDiscs();
			
			_gridModel.addEventListener(GridEvent.CHANGE_CELL, showDisc);
			_gridModel.addEventListener(GridEvent.FOND_POSSIBLE, showPossible);
			this.touchable = false;
		}
		
		private function drawDiscs():void
		{
			_posible = drawCircle(Options.CELL_SIZE*0.1, 0x00ff00);
			
			if(Options.DISCS_COLOR == CellType.BLACK)
			{
				_white = new SimpleImage(assets.getTexture("white"))
				_black = new SimpleImage(assets.getTexture("black"))
			}
			else 
			{
				_white = new SimpleImage(assets.getTexture("black"))
				_black = new SimpleImage(assets.getTexture("white"))
			}
			
			_white.name = "white";
			_white.alignPivot()
			addChild(_white);
			
			_black.name = "black";
			_black.alignPivot()
			addChild(_black);
			
			_black.width = _black.height = _white.height = _white.width = Options.CELL_SIZE*0.75
//			
			_black.x = _white.x = (Options.CELL_SIZE) >> 1;
			_black.y = _white.y = (Options.CELL_SIZE) >> 1;
//			
			_white.visible = _black.visible = false;
			startScale = _white.scaleX;
		}
		
		private function drawCircle(radius:Number, color:Number):Canvas
		{
			var canvas:Canvas;
				canvas = new Canvas();
				canvas.beginFill(color);
				canvas.drawCircle(0, 0, radius);
				addChild(canvas);
			
			canvas.x = (this.width - radius) >> 1;
			canvas.y = (this.height - radius) >> 1;
			canvas.visible = false;
			
			return canvas;
		}
		
		private function drawFrame():void
		{
			with(Options)
			{
				var shape:SimpleShape = new SimpleShape(CELL_SIZE, FRAME_THICKNESS, FRAME_COLOR);
				addChild(shape);
				
				shape = new SimpleShape(FRAME_THICKNESS, CELL_SIZE, FRAME_COLOR);
				addChild(shape); 
			}	
		}
		
		private function showDisc(e:Event):void
		{
			_posible.visible = false;
	
			if(e.data["x"] == _position.x && e.data["y"] == _position.y)
			{
				if(e.data["add"])
				{
					if(e.data["step"] == CellType.BLACK) onAddDiscs(_black);
					else onAddDiscs(_white);
				}
				else
				{
					if(e.data["step"] == CellType.BLACK) onTurnDiscs(_white);
					else onTurnDiscs(_black);			
				}		
			}	
		}
		
		private function onAddDiscs(c:SimpleImage):void
		{
			c.scaleX = startScale;
			c.visible = true;
			c.alpha = 0;
			if(c.name == "black") _white.visible = false;
			else _black.visible = false;
			
			TweenLite.to(c, Animations.ADD_DELAY, { alpha: 1})	
		}
		
		private function onTurnDiscs(c:SimpleImage):void
		{
			
			if(c.name == "black")
			{
				_white.scaleX = scale;
				fun = onTurnWhite;
			}
			else
			{
				_black.scaleX = scale;
				var fun:Function = onTurnBlack;
			}
			
			TweenLite.to(c, Animations.TURN_DELAY, { scaleX: scale, onComplete: fun })
		}
		
		private function onTurnBlack():void
		{
			_white.visible = false;
			_black.visible = true;
			TweenLite.to(_black, Animations.TURN_DELAY, { scaleX: startScale})
		}
		
		private function onTurnWhite():void
		{
			_white.visible = true;
			_black.visible = false;

			TweenLite.to(_white, Animations.TURN_DELAY, { scaleX: startScale})
		}
		
		private function nextStep():void
		{
			AIModel.INSTANCE.makeAIStep(null);
		}
		
		private function showPossible(e:Event):void
		{
			if(e.data["x"] == _position.x && e.data["y"] == _position.y && e.data["step"] == CellType.BLACK)
			{
				_posible.visible = true;
			}
		}
		
		override protected function destroy(e:Event):void
		{
			super.destroy(e);
			_gridModel.removeEventListener(GridEvent.CHANGE_CELL, showDisc);
		}
	}
}