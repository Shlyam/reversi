package view.windows
{
	import tools.display.SimpleImage;
	import tools.display.SimpleSprite;
	import tools.managers.WindowsManager;
	
	import starling.events.Event;
	import starling.events.TouchEvent;
	import starling.text.TextField;
	
	import tools.Sprite2Button;
	
	public class UniversalWindow extends SimpleSprite
	{
		private var _closeBtn:SimpleImage;
		protected var _subscribe:TextField;
		
		public function UniversalWindow()
		{

		}
		
		override protected function init(e:Event = null):void
		{
			super.init(e);

			createBackGround();
			createCloseBtn();
				
			this.alignPivot();	
		}
		
		private function createCloseBtn():void
		{
			_closeBtn = new SimpleImage(assets.getTexture("btn_close"));
			
			addChild(_closeBtn);
			_closeBtn.x = this.width*0.75;
			
			new Sprite2Button(_closeBtn, closeWindow, Sprite2Button.SCALE);
		}
		
		protected function closeWindow(e:TouchEvent):void
		{
			WindowsManager.hide();
		}
		
		private function createBackGround():void
		{
			var subs:SimpleImage = new SimpleImage(assets.getTexture("shield"));
			addChild(subs);
			
			_subscribe = new TextField(subs.width*0.75, 100, "Description", "Verdana",24);
			addChild(_subscribe);
			_subscribe.x = (subs.width - _subscribe.width) >> 1;
			_subscribe.y = subs.height*0.05;
		}
		
		override protected function onResize(e:Event):void
		{
			this.x = stage.stageWidth >> 1;
			this.y = stage.stageHeight >> 1;
		}
		
		override protected function destroy(e:Event):void
		{
			super.destroy(e);
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
		}
	}
}