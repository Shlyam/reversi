package view.windows
{
	import constant.Description;
	
	import tools.managers.WindowsManager;
	
	import starling.display.DisplayObject;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;

	public class DifficultWindow extends UniversalWindow
	{
		private var _easy:TextField;
		private var _medium:TextField;
		private var _hard:TextField;
		
		public function DifficultWindow()
		{
		}
		
		override protected function init(e:Event = null):void
		{
			super.init(e);

			_easy = createButtons("1", "Мартышка");
			_easy.addEventListener(TouchEvent.TOUCH, onTouch);
			_easy.y = this.height*0.1 + _easy.height;
			
			_medium = createButtons("2", "Человек");
			_medium.addEventListener(TouchEvent.TOUCH, onTouch);
			_medium.y = _easy.y + _easy.height;
			
			_hard = createButtons("3", "Робот");
			_hard.addEventListener(TouchEvent.TOUCH, onTouch);
			_hard.y =  _medium.y + _medium.height;
			
			_subscribe.text = Description.DIFFICULT;
			
			onResize(null);
		}
		
		private function onTouch(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(e.currentTarget as DisplayObject);
			(e.currentTarget as DisplayObject).useHandCursor = true;
			
			for each (var touch:Touch in touches)
			{
				 if(touch.phase == TouchPhase.ENDED)
				{
					Options.AI_DIFFICULT = int((e.currentTarget as DisplayObject).name);
					WindowsManager.hide(function ():void{WindowsManager.show(ChooseDiscsWindow);});
					
				}
			}
		}
		
		private function createButtons(buttonName:String, desc:String):TextField
		{
				var btn:TextField = new TextField(180, 50, desc, "Verdana", 26, 0, true);
					addChild(btn);
					btn.name = buttonName;
					btn.x = (this.width - btn.width) >> 1;
			
			return btn;
		}
		
		override protected function destroy(e:Event):void
		{
			super.destroy(e);
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
			
			_easy.removeEventListener(TouchEvent.TOUCH, onTouch);
			_medium.removeEventListener(TouchEvent.TOUCH, onTouch);
			_hard.removeEventListener(TouchEvent.TOUCH, onTouch);

		}
	}
}