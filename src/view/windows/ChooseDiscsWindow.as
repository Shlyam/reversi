package view.windows
{
	import constant.CellType;
	import constant.Description;
	
	import tools.display.SimpleImage;
	import tools.managers.ScreenManager;
	import tools.managers.WindowsManager;
	
	import starling.display.DisplayObject;
	import starling.events.Event;
	import starling.events.TouchEvent;
	
	import tools.Sprite2Button;
	
	import view.GameScreen;

	public class ChooseDiscsWindow extends UniversalWindow
	{
		private var _black:SimpleImage;
		private var _white:SimpleImage;
		
		public function ChooseDiscsWindow()
		{
			super();
		}
		
		override protected function init(e:Event = null):void
		{
			super.init(e);

			_black = new SimpleImage(assets.getTexture("black"));
			createButton(_black, "black", String(CellType.BLACK));
			_black.x = (this.width >> 1) + _black.width;
			
			_white = new SimpleImage(assets.getTexture("white"));
			createButton(_white, "white", String(CellType.WHITE));
			_white.x = (this.width >> 1) - _white.width;
			
			_subscribe.text = Description.DISCS;
			
			onResize(null);
		}
		
		private function createButton(target:SimpleImage, texture:String, targetName:String):void
		{	
			target.scaleX = target.scaleY = 0.75; 
			addChild(target);
			
			target.name = targetName;
			new Sprite2Button(target, onChoose, Sprite2Button.SCALE);
			
			target.y = this.height*0.35;
		}
		
		private function onChoose(e:TouchEvent):void
		{
			Options.DISCS_COLOR = int((e.currentTarget as DisplayObject).name);
			WindowsManager.hide(function ():void{ScreenManager.show(GameScreen)});
		}
		
		
		override protected function destroy(e:Event):void
		{
			super.destroy(e);
		}
	}
}