package view.windows
{
	import constant.Description;
	
	import tools.managers.ScreenManager;
	import tools.managers.WindowsManager;
	
	import starling.events.Event;
	import starling.events.TouchEvent;
	
	import view.GameScreen;

	public class GameOverWindow extends UniversalWindow
	{
		private var _isWin:Boolean;
		public function GameOverWindow(param:Array)
		{
			_isWin = param[0]
		}
		
		override protected function init(e:Event = null):void
		{
			super.init(e);
			
			if(_isWin) _subscribe.text = Description.WIN;
			else _subscribe.text = Description.LOSE;

			onResize(null);
		}
		
		override protected function closeWindow(e:TouchEvent):void
		{
			WindowsManager.hide(function ():void{ScreenManager.show(GameScreen)});
		}
		
		
		override protected function destroy(e:Event):void
		{
			super.destroy(e);
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
		}
	}
}