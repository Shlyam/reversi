package view
{
	import starling.display.DisplayObject;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	import starling.text.TextField;
	
	import tools.display.SimpleImage;
	import tools.display.SimpleSprite;
	import tools.managers.WindowsManager;
	
	import view.windows.DifficultWindow;
	
	public class MainScreen extends SimpleSprite
	{
		private var _backGround:SimpleImage;
		private var _startButton:TextField;
		private var _greeting:TextField;
		
		public function MainScreen()
		{
		}
		
		override protected function init(e:Event = null):void
		{
			super.init(e);
			
			_backGround = new SimpleImage(assets.getTexture("background"));
			addChild(_backGround);
			
			_greeting = new TextField(stage.stageWidth, 100, "REVERSI", "Verdana", 48, 0x0, true)
			addChild(_greeting)
			_greeting.y = stage.stageHeight*0.2;
			
			createButton();

			onResize(null);
		}
		
		private function createButton():void
		{
			_startButton = new TextField(220, 50, "Новая игра", "Verdana", 18, 0, true);
			addChild(_startButton);
			_startButton.alignPivot();
			_startButton.addEventListener(TouchEvent.TOUCH, onTouch);
			
			_startButton.x = stage.stageWidth >> 1;
			_startButton.y = stage.stageHeight >> 1;
		}
		
		private function onTouch(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(e.currentTarget as DisplayObject);
			(e.currentTarget as DisplayObject).useHandCursor = true;
			
			for each (var touch:Touch in touches)
			{
				if(touch.phase == TouchPhase.ENDED)
				{
					WindowsManager.show(DifficultWindow);
				}
			}
		}
		
		override protected function onResize(e:Event):void
		{
			_backGround.width = stage.stageWidth;
			_backGround.height = stage.stageHeight;
		}
		
		override protected function destroy(e:Event):void
		{
			super.destroy(e);
			removeEventListener(Event.REMOVED_FROM_STAGE, destroy);	
		}
	}
}