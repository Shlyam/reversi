package view
{
	import constant.CellType;
	
	import tools.display.SimpleImage;
	import tools.display.SimpleSprite;
	import tools.managers.ScreenManager;
	
	import events.GridEvent;
	
	import model.GridModel;
	
	import starling.display.Image;
	import starling.events.Event;
	import starling.events.TouchEvent;
	import starling.text.TextField;
	
	import tools.Sprite2Button;
	
	public class TopPanel extends SimpleSprite
	{
		private var _back:SimpleImage;
		private var _restart:SimpleImage;
		
		private var _gridModel:GridModel;
		
		private var _blackScore:TextField;
		private var _whitekScore:TextField;
		
		private var _blackView:SimpleSprite;
		private var _whiteView:SimpleSprite;
		
		//align
		private const offset:Number = 10;
		
		public function TopPanel(gridModel:GridModel)
		{
			_gridModel = gridModel;
		}
		
		override protected function init(e:Event = null):void
		{
			super.init(e);

			createButtons();
			createScoreBoard();
			_gridModel.addEventListener(GridEvent.CHANGE_CELL, onChange);
			onResize(null);
		}
		
		private function createScoreBoard():void
		{
			
			var scoreBoard:SimpleSprite = new SimpleSprite();
			addChild(scoreBoard);
			
			_blackScore = new TextField(30, 20, "0")
			_whitekScore = new TextField(30, 20, "0")
			
			var subs:SimpleImage = new SimpleImage(assets.getTexture("shield_decor"));
				subs.scaleX = subs.scaleY = 0.5;
				scoreBoard.addChild(subs);
				
				if(Options.DISCS_COLOR == CellType.BLACK)
				{
					_blackView = scoreView(_blackScore, "black"); 
					_whiteView = scoreView(_whitekScore, "white");
				}
				else 
				{
					_blackView = scoreView(_blackScore, "white"); 
					_whiteView = scoreView(_whitekScore, "black");
				}
				
				_blackView.x = offset;
				_blackView.y = (subs.height - _blackView.height) >> 1;
				
				
				_whiteView.x = subs.width - _whiteView.width - offset;
				_whiteView.y = (subs.height - _whiteView.height) >> 1;
				
			scoreBoard.addChild(_blackView);
			scoreBoard.addChild(_whiteView);
			
			var vs:SimpleImage = new SimpleImage(assets.getTexture("VS"));
				scoreBoard.addChild(vs);
				vs.scaleX = vs.scaleY = 0.5;
				vs.x = (subs.width - vs.width) >> 1;
				vs.y = (subs.height - vs.height) >> 1;
			
			
			scoreBoard.x = stage.stageWidth >> 1;
			scoreBoard.x = (stage.stageWidth - scoreBoard.width) >> 1;
		}
		
		private function onChange(e:Event):void
		{
			
			if(e.data.add)
			{
				_blackScore.text = String(_gridModel.blackCount);
				_whitekScore.text = String(_gridModel.whiteCount);
			}
		}
		
		private function scoreView(tf:TextField, texture:String):SimpleSprite
		{
			var sub:SimpleSprite = new SimpleSprite();
			var img:SimpleImage = new SimpleImage(assets.getTexture(texture));
				sub.addChild(img);
				sub.addChild(tf);
				img.scaleX = img.scaleY = 0.35;
				
				tf.x = (img.width - tf.width) >> 1;
				tf.y = (img.height - tf.height) >> 1;

				return sub;
		}
		
		private function createButtons():void
		{
			_back = new SimpleImage(assets.getTexture("btn_next"));
			addChild(_back)
			new Sprite2Button(_back, toMap);

			
			_restart = new SimpleImage(assets.getTexture("btn_replay"));
			addChild(_restart)
			new Sprite2Button(_restart, restart);
			_restart.x = stage.stageWidth - (_restart.width >> 1); 
		}
		
		private function toMap(e:TouchEvent):void
		{
			ScreenManager.show(MainScreen);
		}		
		
		private function restart(e:TouchEvent):void
		{
			ScreenManager.show(GameScreen);
		}		
		
		
		override protected function onResize(e:Event):void{}
		
		override protected function destroy(e:Event):void
		{
			super.destroy(e);
			_gridModel.removeEventListener(GridEvent.CHANGE_CELL, onChange);
		}
	}
}