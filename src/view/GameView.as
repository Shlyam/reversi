package view
{
	import com.greensock.TweenLite;
	
	import flash.geom.Point;
	
	import ai.AIModel;
	
	import constant.Animations;
	import constant.CellType;
	
	import tools.display.SimpleShape;
	import tools.display.SimpleSprite;
	
	import events.GridEvent;
	
	import model.GridModel;
	
	import starling.animation.Tween;
	import starling.display.DisplayObject;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.events.TouchPhase;
	
	public class GameView extends SimpleSprite
	{
		private var _gridModel:GridModel;
		
		private var _tapArea:SimpleShape;
		private var _background:SimpleSprite;
		
		public function GameView(gridModel:GridModel)
		{
			_gridModel = gridModel;
		}
		
		override protected function init(e:Event = null):void
		{
			super.init(e);

			createTapArea();
			createBackground();
			
			alignPivot();
			
			_gridModel.addEventListener(GridEvent.CHANGE_CELL, onChange);
			
			onResize(null);
		}
		
		private function onChange(e:Event):void
		{
			if(e.data.add && !e.data.isStart) TweenLite.to(this, Animations.TURN_DELAY + Animations.ADD_DELAY, {onComplete: onComplete});
		}
		private function onComplete():void
		{
			_gridModel.findPossible();
			if(_gridModel.currentStep == CellType.WHITE) AIModel.INSTANCE.makeAIStep(null);
		}
		private function createBackground():void
		{
			_background = new SimpleSprite();
			addChild(_background);
			var cell:CellView;
			for (var i:int = 0; i < Options.WIDTH; i++) 
			{
				for (var j:int = 0; j < Options.HEIGHT; j++) 
				{
					cell = new CellView(_gridModel, i, j);
					_background.addChild( cell );
					cell.x = i*Options.CELL_SIZE;
					cell.y = j*Options.CELL_SIZE;
				}
			}			
		}
		
		private function createTapArea():void
		{	
			_tapArea = new SimpleShape(Options.WIDTH*Options.CELL_SIZE, Options.HEIGHT*Options.CELL_SIZE, Options.FIELD_COLOR);
			addChild(_tapArea);
			_tapArea.addEventListener(TouchEvent.TOUCH, onTouch)
		}
		
		private function onTouch(e:TouchEvent):void
		{
			var touches:Vector.<Touch> = e.getTouches(e.currentTarget as DisplayObject);
			(e.currentTarget as DisplayObject).useHandCursor = true;
			
			for each (var touch:Touch in touches)
			{
				if(touch.phase == TouchPhase.ENDED) onTap( local2Grid(touch.getLocation(this)) );
			}
		}
		
		private function onTap(p:Point):void
		{
			if(p.x >= 0 && p.x < Options.WIDTH && p.y >= 0 && p.y < Options.HEIGHT)
			_gridModel.todoStep(p.x, p.y);
		}
		
		private function local2Grid(localPoint:Point):Point
		{
			return new Point( 	int(localPoint.x/Options.CELL_SIZE),
								int(localPoint.y/Options.CELL_SIZE));
		}
		
		override protected function onResize(e:Event):void
		{
			this.x = stage.stageWidth >> 1;
			this.y = stage.stageHeight >> 1;
		}
		
		override protected function destroy(e:Event):void
		{
			super.destroy(e);
			_gridModel.addEventListener(GridEvent.CHANGE_CELL, onChange);
		}
	}
}