package view
{
	import ai.AIModel;
	
	import tools.display.SimpleImage;
	import tools.display.SimpleSprite;
	import tools.managers.WindowsManager;
	
	import events.GridEvent;
	
	import model.GridModel;
	
	import starling.events.Event;
	
	import view.windows.GameOverWindow;

	public class GameScreen extends SimpleSprite
	{
		private var _gridModel:GridModel;
		private var _aiModel:AIModel;
		
		private var _gameView:GameView;
		private var _topPanel:TopPanel;
		
		private var _backGround:SimpleImage;
		
		public function GameScreen()
		{
		}
		
		override protected function init(e:Event = null):void
		{
			super.init(e);
			
			_backGround = new SimpleImage(assets.getTexture("background"));
			addChild(_backGround);
			
			_gridModel = new GridModel();
			_aiModel = new AIModel(_gridModel);
			
			_gameView = new GameView(_gridModel);
			addChild(_gameView);
			
			_topPanel = new TopPanel(_gridModel);
			addChild(_topPanel);
			
			_gridModel.initialize();
				
			_gridModel.addEventListener(GridEvent.GAME_OVER, onGameOver);
			onResize(null);
		}
		
		private function onGameOver(e:Event):void
		{
			
			WindowsManager.show(GameOverWindow, e.data.isWin);
		}
		
		override protected function onResize(e:Event):void
		{
			_backGround.width = stage.stageWidth;
			_backGround.height = stage.stageHeight;
		}
		
		override protected function destroy(e:Event):void
		{
			super.destroy(e);
			_aiModel.removeListeners();
			_gridModel.removeEventListener(GridEvent.GAME_OVER, onGameOver);
		}
	}
}