package model
{
	import flash.geom.Point;
	
	import constant.CellType;
	
	import events.GridEvent;
	
	import starling.events.Event;
	

	public class AIGridModel extends GridModel
	{
		public var bestPoint:Point;
		
		public function AIGridModel()
		{
			super();
			initialize();
		}
		
		public function copyGrid(grid:Vector.<Vector.<uint>>):void
		{
			for (var i:int = 0; i < Options.HEIGHT; i++) 
			{
				for (var j:int = 0; j < Options.WIDTH; j++) 
				{
					_grid[i][j] = grid[i][j];
				}
			}
			console();
		}
		
		override public function todoStep(x:uint, y:uint):void
		{		
			if(_isStart || _grid[y][x] == CellType.POSSIBLE)
			{
				_grid[y][x] = _currentStep;
				if(!_isStart) reversCells(x, y);

				findPossible()
			}	
		}
		
		private var _noSteps:Boolean = false;
		override public function showPossible(currentStep:uint):void
		{
			clearPossible();
			_possibleFinder.find(grid, currentStep);	
			
			if(!_isStart && _possibleFinder.possibleTurnList.length == 0)
			{
				if(_noSteps) return;

				_noSteps = true;
				turnStep();
				clearPossible();
				showPossible(_currentStep);
			}
			else
			{
				_noSteps = false;
				findBest();
			}
		}
		
		public var allPoints:Vector.<Point> = new Vector.<Point>();
		public var max:uint = 0;
		private function findBest():void
		{
			allPoints = new Vector.<Point>();
			max = 0;
			
			var count:int;
			var countSteps:int = _possibleFinder.possibleTurnList.length;
			max = countSteps;
			for (var i:int = 0; i < countSteps; i++) 
			{
				count = _possibleFinder.possibleTurnList[i].length;
				allPoints.push(_possibleFinder.possibleTurnList[i][count - 1]);
			}
		}
		
		override public function turnStep():void
		{
			_enemyStep = inversion(_enemyStep);
			_currentStep = inversion(_currentStep);
		}
	}
}