package model
{
	import flash.geom.Point;
	
	import algorithm.PossibleFinder;
	
	import constant.CellType;
	
	import events.GridEvent;
	
	import starling.events.Event;
	import starling.events.EventDispatcher;
	
	public class GridModel extends EventDispatcher
	{
		static public var CURRENT_COUNT_STEP:int = 0
		protected var _possibleFinder:PossibleFinder;
		
		protected var _grid:Vector.<Vector.<uint>>;
		protected var _currentStep:uint;
		protected var _enemyStep:uint;
		
		private var _blackCount:uint = 0;
		private var _whiteCount:uint = 0;
		
		protected var _isStart:Boolean;
		
		public function GridModel()
		{
			super();
		}
		
		public function get whiteCount():uint
		{
			return _whiteCount;
		}

		public function get blackCount():uint
		{
			return _blackCount;
		}

		public function get enemyStep():uint
		{
			return _enemyStep;
		}

		public function get currentStep():uint
		{
			return _currentStep;
		}

		public function get isStart():Boolean
		{
			return _isStart;
		}

		public function get grid():Vector.<Vector.<uint>>
		{
			return _grid;
		}

		public function initialize():void
		{
			_possibleFinder = new PossibleFinder();
			
			_currentStep = CellType.BLACK;
			_enemyStep = CellType.WHITE;
			
			createGrid();
			addStartCell();
			
		}
		
		protected function createGrid():void
		{
			_grid = new Vector.<Vector.<uint>>();
			
			for (var i:int = 0; i < Options.HEIGHT; i++) 
			{
				_grid.push(new Vector.<uint>());
				
				for (var j:int = 0; j < Options.WIDTH; j++) 
				{
					_grid[i].push(CellType.EMPTY);
				}
			}	
			
			_isStart = true;
		}
		
		protected function addStartCell():void
		{
			todoStep((Options.WIDTH >> 1) - 1, (Options.HEIGHT >> 1)); 
			todoStep((Options.WIDTH >> 1), (Options.HEIGHT >> 1)); 
			todoStep((Options.WIDTH >> 1), (Options.HEIGHT >> 1) - 1); 
			todoStep((Options.WIDTH >> 1) - 1, (Options.HEIGHT >> 1) - 1); 
			_isStart = false;
			
			console();
		}
		
		public function todoStep(x:uint, y:uint):void
		{		
			//trace("todoStep(" + x + ", " + y + ");"); 
			if(_isStart || _grid[y][x] == CellType.POSSIBLE)
			{
				CURRENT_COUNT_STEP++;
				_grid[y][x] = _currentStep;
				if(!_isStart) reversCells(x, y);
				
				countScore();
				dispatchEvent(new Event(GridEvent.CHANGE_CELL, false, {"x" : x, "y" : y, "step" : _currentStep, "add" : true, "isStart" : _isStart}));
				
				if(_isStart)  findPossible()
				console();
			}	
		}
		
		public function findPossible():void
		{
			turnStep();
			clearPossible();
			showPossible(_currentStep);
		}
		
		protected function reversCells(x:uint, y:uint):void
		{	
			var len:uint = _possibleFinder.possibleTurnList.length;
			var pt:Point;
			var count:uint;
			for (var i:int = 0; i < len; i++) 
			{
				count = _possibleFinder.possibleTurnList[i].length;
				pt = _possibleFinder.possibleTurnList[i][count - 1];
				
				if(pt.x == y && pt.y == x)
				{
					for (var j:int = 0; j < count; j++) 
					{		
						pt = _possibleFinder.possibleTurnList[i][j];
						_grid[pt.x][pt.y] = _currentStep;
						dispatchEvent(new Event(GridEvent.CHANGE_CELL, false, {"x" : pt.y, "y" : pt.x, "step" : _currentStep, "add" : false}));
					}
				}	
			}
		}
		
		private var _noSteps:Boolean = false;
		public function showPossible(currentStep:uint):void
		{
			_possibleFinder.find(grid, currentStep);		
			
			for (var i:int = 0; i < Options.HEIGHT; i++) 
			{
				for (var j:int = 0; j < Options.WIDTH; j++) 
				{
					if(_grid[i][j] == CellType.POSSIBLE)
					{
						dispatchEvent(new Event(GridEvent.FOND_POSSIBLE, false, {"x" : j, "y" : i, "step" : _currentStep}));
					}
				}
			}
			
			if(!_isStart && _possibleFinder.possibleTurnList.length == 0)
			{
				if(_noSteps)
				{
					if(_blackCount > _whiteCount) var isWin:Boolean = true;
					else isWin = false;
					
					dispatchEvent(new Event(GridEvent.GAME_OVER, false, {"isWin" : isWin}));
					return;
				}
				
				_noSteps = true;
				findPossible();
			}			
		}
		
		protected function clearPossible():void
		{
			for (var i:int = 0; i < Options.HEIGHT; i++) 
			{
				for (var j:int = 0; j < Options.WIDTH; j++) 
				{
					if(_grid[i][j] == CellType.POSSIBLE) _grid[i][j] = CellType.EMPTY;
				}
			}
		}
		
		public function turnStep():void
		{
			_enemyStep = inversion(_enemyStep);
			_currentStep = inversion(_currentStep);
			
			if(_currentStep == CellType.BLACK) trace("PLAYER STEP")
			else trace("CPU STEP")
		}
		
		static public function inversion(type:uint):uint
		{
			if(type == CellType.BLACK) return CellType.WHITE;
			else return CellType.BLACK;
		}
		
		private function countScore():void
		{
			var white:uint = 0;
			var black:uint = 0;
			for (var i:int = 0; i < Options.HEIGHT; i++) 
			{
				for (var j:int = 0; j < Options.WIDTH; j++) 
				{
					if(_grid[i][j] == CellType.WHITE) white++;
					if(_grid[i][j] == CellType.BLACK) black++;
				}
			}
			
			_whiteCount = white;
			_blackCount = black;
		}
		
		protected function console():void
		{
			return;
			var str:String = '';
			
			for (var i:int = 0; i < Options.HEIGHT; i++) 
			{
				for (var j:int = 0; j < Options.WIDTH; j++) 
				{
					if( _grid[i][j]) str = str + _grid[i][j] + " ";
					else str = str + '.' + " ";
				}
				str = str + "\n";
			}
			
			trace(str);
			trace('----------------------');
		}
	}
}