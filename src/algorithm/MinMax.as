package algorithm
{
	import flash.geom.Point;
	import flash.utils.getTimer;
	
	import constant.CellType;
	
	import model.AIGridModel;
	import model.GridModel;

	public class MinMax
	{
		static public var MAX_DEPTH:uint = 11;
		static public const MAX_THINK_TIME:int = 5000;
		
		private var _gridModel:AIGridModel;
		private var _currentStep:uint;
			
		private var _currentDepth:int = 0;
		
		
		private var _logThinking:Boolean = false;
		private var _depthFields:Vector.<Vector.<Vector.<uint>>> = new Vector.<Vector.<Vector.<uint>>>();
		private var _max:int = 0;
		
		private var _tempBestPath:Vector.<Point> = new Vector.<Point>();
		private var _bestPath:Vector.<Point> = new Vector.<Point>();
		private var thinkTime:int;
		
		public function MinMax()
		{
			_gridModel = new AIGridModel();
			_currentStep = CellType.BLACK;
		}
		
		public function copyGrid(grid:Vector.<Vector.<uint>>):Vector.<Vector.<uint>>
		{
			var _grid:Vector.<Vector.<uint>> = new Vector.<Vector.<uint>>();
			
			for (var i:int = 0; i < Options.HEIGHT; i++) 
			{
				_grid.push(new Vector.<uint>());
				
				for (var j:int = 0; j < Options.WIDTH; j++) 
				{
					_grid[i].push(grid[i][j]);
				}
			}

			return _grid;
		}

		
		public function startFind(currentStep:uint, grid:Vector.<Vector.<uint>>, stepPoint:Point = null, lastMax:int = 0, lastMin:int = 0):Point
		{
			if(MAX_DEPTH > 3 && (MAX_DEPTH >= Options.WIDTH*Options.HEIGHT - GridModel.CURRENT_COUNT_STEP - 1)) MAX_DEPTH -= 2;
			
			_max = 0;
			_depthFields = new Vector.<Vector.<Vector.<uint>>>();
			_tempBestPath = new Vector.<Point>();
			thinkTime = getTimer();
			_logThinking = false;
			
			_gridModel.copyGrid(grid);
			_gridModel.showPossible(currentStep);
			
			if(Options.AI_DIFFICULT == 2)
			{
				for each (var p:Point in  _gridModel.allPoints) 
				{
					if(checkCorner(p)) return p;
				}
			}
			
			var bestPoint:Point = findStep(currentStep, grid);
			return bestPoint;
		}
		
		public function findStep(currentStep:uint, grid:Vector.<Vector.<uint>>, stepPoint:Point = null, lastMax:int = 0, lastMin:int = 0):Point
		{	
			if((_max != 0 && _currentDepth < MAX_DEPTH) || _logThinking) return _bestPath[0];
			
			_currentDepth++;
			if(!stepPoint)			
			{
				_gridModel.turnStep();
				_gridModel.copyGrid(grid);
				_gridModel.showPossible(currentStep);
				
				_depthFields.push(copyGrid(grid));
	
				if(currentStep == CellType.WHITE) lastMax += _gridModel.max;

				for each (var p:Point in  _gridModel.allPoints) 
				{
					findStep(currentStep, _depthFields[0], p, lastMax, lastMin);
				}
				
				if(_gridModel.currentStep == CellType.WHITE ) _gridModel.turnStep();
			}
			else
			{
				_gridModel.copyGrid(grid);
				_gridModel.showPossible(currentStep);			
				_gridModel.todoStep(stepPoint.y, stepPoint.x);
				
				_depthFields.push(copyGrid(_gridModel.grid));
				_tempBestPath.push(stepPoint.clone());		
				
				if(currentStep == CellType.BLACK) lastMax += _gridModel.max;
				else  lastMin += _gridModel.max;
				
				if(_currentDepth == MAX_DEPTH)
				{					
					if(thinkTime  + MAX_THINK_TIME < getTimer())
					{
						_logThinking = true;
						clonePath(_tempBestPath);
					}
					if(_max < lastMax - lastMin)
					{
						_max = lastMax - lastMin;
						clonePath(_tempBestPath);			
					}
				}
				
				for each (p in  _gridModel.allPoints) 
				{
					if(_currentDepth < MAX_DEPTH) findStep(_gridModel.currentStep, _depthFields[_currentDepth - 1], p, lastMax, lastMin);
				}
				_gridModel.turnStep();	
			}
			
			_depthFields.pop();
			_tempBestPath.pop();
			
			if(currentStep == CellType.BLACK) lastMax -= _gridModel.max;
			else  lastMin -= _gridModel.max;
			
			_currentDepth--;
			
			if(_bestPath.length) return _bestPath[0];
			else return stepPoint;
		}
		
		private function checkCorner(pt:Point):Boolean
		{
			if(	(pt.x == 0 && pt.y == 0) ||
				(pt.x == Options.WIDTH - 1 && pt.y == Options.HEIGHT - 1) ||
				(pt.x == 0 && pt.y == Options.WIDTH - 1) ||
				(pt.x == 0 && pt.y == Options.HEIGHT - 1)) return true;
			return false;
		}
		
		private function clonePath(path:Vector.<Point>):void
		{
			_bestPath = new Vector.<Point>();
			for (var i:int = 0; i < path.length; i++) 
			{
				_bestPath.push(path[i]);
			}
			
		}
		
	}
}