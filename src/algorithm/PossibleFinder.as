package algorithm
{
	import flash.geom.Point;
	
	import constant.CellType;
	
	import model.GridModel;

	public class PossibleFinder
	{
		private var _grid:Vector.<Vector.<uint>>;
		private var _possibleTurnList:Vector.<Vector.<Point>>
		private var _turning:Vector.<Point>;
		
		public function PossibleFinder()
		{
			_possibleTurnList = new Vector.<Vector.<Point>>();
			_turning = new Vector.<Point>();
		}
		
		public function get possibleTurnList():Vector.<Vector.<Point>>
		{
			return _possibleTurnList;
		}

		private function setPossible(possible:Point, check:Point):void
		{
			_grid[possible.x][possible.y] = CellType.POSSIBLE;
			//_turning.unshift(check);
			_possibleTurnList.push(_turning);
		}
		
		public function find(grid:Vector.<Vector.<uint>>, step:uint):void
		{		
			_grid = grid;
			_possibleTurnList = new Vector.<Vector.<Point>>();
			for (var i:int = 0; i < Options.HEIGHT; i++) 
			{
				for (var j:int = 0; j < Options.WIDTH; j++) 
				{
					if(grid[i][j] == step) 
					{
						//right
						_turning = new Vector.<Point>();
						for (var k:int = j + 1; k < Options.WIDTH; k++) 
						{
							if(grid[i][k] == step) break;
							_turning.push(new Point(i, k));
							if(grid[i][k] == CellType.EMPTY || grid[i][k] == CellType.POSSIBLE)
							{
								if(grid[i][k - 1] == GridModel.inversion(grid[i][j])) setPossible(new Point(i, k), new Point(i, j));
								break;
							}
						}
						
						//left
						_turning = new Vector.<Point>();
						for (k = j - 1; k >= 0; k--) 
						{
							if(grid[i][k] == step) break;
							_turning.push(new Point(i, k))
							if(grid[i][k] == CellType.EMPTY || grid[i][k] == CellType.POSSIBLE)
							{
								if(grid[i][k + 1] == GridModel.inversion(grid[i][j])) setPossible(new Point(i, k), new Point(i, j));
								break;
							}
						}
						
						//up
						_turning = new Vector.<Point>();
						for (k = i - 1; k >= 0; k--) 
						{
							if(grid[k][j] == step) break;
							_turning.push(new Point(k, j))
							if(grid[k][j] == CellType.EMPTY || grid[k][j] == CellType.POSSIBLE)
							{
								if(grid[k + 1][j] == GridModel.inversion(grid[i][j])) setPossible(new Point(k, j), new Point(i, j));
								break;
							}
						}
						
						//down
						_turning = new Vector.<Point>();
						for (k = i + 1; k < Options.HEIGHT; k++) 
						{
							if(grid[k][j] == step) break;
							_turning.push(new Point(k, j))
							if(grid[k][j] == CellType.EMPTY || grid[k][j] == CellType.POSSIBLE)
							{
								if(grid[k - 1][j] == GridModel.inversion(grid[i][j])) setPossible(new Point(k, j), new Point(i, j));
								break;
							}
						}
						
						//right-up
						e = 0;
						_turning = new Vector.<Point>();
						for (k = j + 1; k < Options.WIDTH; k++) 
						{
							e++;
							if(i - e >= 0 )
							{
								if(grid[i - e][k]  == step) break;
								_turning.push(new Point(i - e, k))
								if( grid[i - e][k] == CellType.EMPTY || grid[i - e][k] == CellType.POSSIBLE)
								{
									if(grid[i - e + 1][k - 1] == GridModel.inversion(grid[i][j])) setPossible(  new Point(i - e, k), new Point(i, j));
									break;
								}
							}
						}
						
						//left-up
						var e:int = 0;
						_turning = new Vector.<Point>();
						for (k = j - 1; k >= 0; k--) 
						{
							e++;
							if(i - e >= 0)
							{
								if(grid[i - e][k]  == step) break;
								_turning.push(new Point(i - e, k))
								if( grid[i - e][k] == CellType.EMPTY || grid[i - e][k] == CellType.POSSIBLE)
								{
									if(grid[i - e + 1][k + 1] == GridModel.inversion(grid[i][j])) setPossible(new Point(i - e, k), new Point(i, j));
									
									break;
								}
							}	
						}

						//right-down
						e = 0;
						_turning = new Vector.<Point>();
						for (k = j + 1; k < Options.WIDTH; k++) 
						{
							e++;
							if(i + e < Options.HEIGHT)
							{
								if(grid[i + e][k]  == step) break;
								_turning.push(new Point(i + e, k))
								if( grid[i + e][k] == CellType.EMPTY || grid[i + e][k] == CellType.POSSIBLE)
								{
									if(grid[i + e - 1][k - 1] == GridModel.inversion(grid[i][j])) setPossible( new Point(i + e, k), new Point(i, j))
									break;
								}
							}						
						}
						
						//left-down
						e = 0;
						_turning = new Vector.<Point>();
						for (k = j - 1; k >= 0; k--) 
						{
							e++;
							if(i + e < Options.HEIGHT)
							{
								if(grid[i + e][k]  == step) break;
								_turning.push(new Point(i + e, k))
								if( grid[i + e][k] == CellType.EMPTY || grid[i + e][k] == CellType.POSSIBLE)
								{
									if(grid[i + e - 1][k + 1] == GridModel.inversion(grid[i][j])) setPossible(  new Point(i + e, k), new Point(i, j));
									break;
								}
							}
						}
					}
				}
			}
			
		}
	}
}