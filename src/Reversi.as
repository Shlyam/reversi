package
{
	import flash.display.Sprite;
	import flash.net.URLLoaderDataFormat;
	
	import Assets.Content;
	
	import tools.managers.ScreenManager;
	
	import tools.loaders.SALoaderUrl;
	
	import view.MainScreen;
	
	[SWF(width="800", height="600", frameRate="60", backgroundColor = "0x8B4726")]
	
	public class Reversi extends Sprite
	{
		public function Reversi()
		{		
			new SALoaderUrl("../assets/Options.xml", onXMLLoad, URLLoaderDataFormat.TEXT);
		}
		
		private function onXMLLoad(data:*):void
		{
			var optionsXML:XML = XML(data);
			
			Options.WIDTH = optionsXML.fieldWidth;
			Options.HEIGHT = optionsXML.fieldHeight;
			
			if(Options.WIDTH < 5) Options.WIDTH = 5;
			if(Options.HEIGHT < 5) Options.HEIGHT = 5;
			
			Options.FIELD_COLOR = optionsXML.fieldColor;
			Options.FRAME_COLOR = optionsXML.frameColor;
			Options.CELL_SIZE = optionsXML.cellSize;
			
			initEngine();
		}
		
		private function initEngine():void
		{
			Connections.STATIC_PATH = "../assets/"
			
			Content.atfList = ["assets"];
			
			appMain.loadComplete = onComplete;
			
			new StarlingManager(stage);
		}
		
		private function onComplete():void
		{
			ScreenManager.show(MainScreen);

		}
	}
}