package constant
{
	public class Description
	{
		static public const DIFFICULT:String = "Выбирете сложность";
		static public const DISCS:String = "Какой будем играть?";
		static public const WIN:String = "Вы выиграли";
		static public const LOSE:String = "Вы проиграли";
	}
}