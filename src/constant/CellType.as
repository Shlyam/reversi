package constant
{
	public class CellType
	{
		static public const EMPTY:uint = 0;
		static public const POSSIBLE:uint = 1;
		static public const WHITE:uint = 2;
		static public const BLACK:uint = 3;
	}
}